Running scripts
===============


In this folder you will find the scripts necceary to run the code.
First thing to do is to choose which case you want to run.


For example you want to run ``CASE3`` with a 60 degree fiber angle using the coarse mesh and the acitve strain formulation.
Then you set

.. code-block:: python

    angle = 60
    patient = "CASE3"
    resolution = "low"
    active_model = "active_strain"


in the ``run.py`` file.

You can also have a look in the ``params.py`` file for additional options. To see all the available options please consult the `Pulse-Adjoint <https://bitbucket.org/finsberg/pulse_adjoint>`_ repository.

Once you are happy with the setup you can do

.. code-block:: bash

    python run.py


The output files with be saved to the path ``results_<active_mode>/patient_<patient>/res_<resolution>/fiber_<angle>``, which, by the way, can be changed in ``params.py``.
Once the simulation is done you will have the following files created:


* results.h5 - this is the file containing the results

* output.log - this is a file containing all the logging

* input.yml - this is a file containing all the settings used for this simulation


To postprocess the results, open the script called ``postprocess.py``, and set e.g

.. code-block:: python

    angle = 60
    patient = "CASE3"
    resolution = "low"
    active_model = "active_strain"



in the ``main`` function. Then run


.. code-block:: bash

    python postprocess.py


This will create some new files. For more options on postprocessing have a look at the submodule called ``Postprocess`` in the `Pulse-Adjoint <https://bitbucket.org/finsberg/pulse_adjoint>`_ repository.

