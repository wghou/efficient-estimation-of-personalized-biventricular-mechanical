import os


patients = ["CASE1", "CASE2", "CASE3"]

filepath= os.path.dirname(os.path.abspath(__file__))

OUTPATH = os.path.join(filepath, "results_{}/patient_{}/res_{}/fiber_{}")

pv_path = os.path.join(filepath,"../data/pv_data/{}.yml")
mesh_path = os.path.join(filepath, "../data/mesh/{}_{}_{}.h5")

angles = [30,40,50,60,70,80,90]
    
active_models = ["active_strain", "active_stress"]

resolutions = ["low", "med"]

es_points_ = {"CASE1":18,
             "CASE2": 13,
             "CASE3": 13}

es_points = {}
for ac in active_models:
    for p in patients:
        for a in angles:
            for r in resolutions:
                key = "{}:{}:{}:{}".format(ac, p, r, a)
                es_points[key] = es_points_[p]
    

def get_params(a=1.291):

    from pulse_adjoint.setup_optimization import setup_adjoint_contraction_parameters

    
    ### Fixed for all runs ###
    opttargets = {"volume":True,
                  "rv_volume": True,
                  "regional_strain":True,
                  "full_strain":False,
                  "GL_strain":False,
                  "GC_strain":False,
                  "displacement":False}
    
    optweight_active = {"volume":0.1,
                        "rv_volume": 0.1,
                        "regional_strain": 1.0, 
                        "regularization": 1e-4}
    optweight_passive = {"volume":1.0,
                         "rv_volume": 1.0,
                         "regularization": 1e-6}

    params = setup_adjoint_contraction_parameters()
  
    
    params["gamma_space"] = "regional"
    params["adaptive_weights"] = False
    params["passive_weights"] = "-1"
    params["base_spring_k"] = 0.0
    params["pericardium_spring"] = 0.5
    params["strain_reference"] = "ED"
    params["strain_tensor"] = "E"        
    params["active_relax"] = 1.0
    
    # Marge lv (0) and septum (1)
    params["merge_passive_control"] = "0,1"
    params["initial_guess"] = "previous"
    
    params["optimize_matparams"] = True
    params["verbose"] =False
    params["log_level"] = 20
    params["matparams_space"] = "regional"
    params["unload"] = True

    params["Optimization_parameters"]["active_opt_tol"] = 1e-8
    
    params["Optimization_parameters"]["matparams_min"] = 0.05
    params["Optimization_parameters"]["fixed_matparams"] = ""
    params["Optimization_parameters"]["matparams_max"] = 50.0
    params["Optimization_parameters"]["gamma_max"] = 1.0
    
    params["Unloading_parameters"]["estimate_initial_guess"] = False
    params["Unloading_parameters"]["maxiter"] = 1
    params["Unloading_parameters"]["tol"] = 1e-3
    params["Unloading_parameters"]["continuation"] = False
    params["Unloading_parameters"]["method"] = "raghavan"
    params["Unloading_parameters"]["unload_options"]["maxiter"] = 1
    params["Unloading_parameters"]["unload_options"]["ub"] = 1.2
    params["Optimization_parameters"]["passive_maxiter"] = 20
    
    
    for k, v in opttargets.iteritems():
        params["Optimization_targets"][k] = v
    
        
    for k, v in optweight_active.iteritems():
        params["Active_optimization_weigths"][k] = v
    
    for k, v in optweight_passive.iteritems():
        params["Passive_optimization_weigths"][k] = v
    
    params["Patient_parameters"]["patient_type"] = "full"
    params["Patient_parameters"]["mesh_type"] = "biv"

    
    params["Material_parameters"]["a"] = a
    params["Material_parameters"]["a_f"] = 2.582
    params["Material_parameters"]["b"] = 5.0
    params["Material_parameters"]["b_f"] = 5.0
    

    return params
